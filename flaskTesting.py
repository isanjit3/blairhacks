from flask import Flask, flash, redirect, render_template, request, session, abort
app = Flask(__name__)

@app.route("/") #sectioned into directories that you want to go into
@app.route("/")
def index():
    return "Index"


@app.route("/hello")
def hello():
    return "<strong>Hello world!</strong>"


@app.route("/members")
def members():
    return "Members"


# @app.route("/members/<string:name>/")
# def getMember(name):
#     return name </string:name >

@app.route("/hello/<string:name>/")
def hello(name):
    return render_template(
        'test.html', name=name) < / string: name >


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)

if __name__ == "__main__":
    app.run()
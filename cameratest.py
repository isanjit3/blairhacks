from PIL import Image
import pytesseract
import numpy as np
import cv2
import enchant


rawFile = open("transcription.txt", "w")
finalFile = open("finalText.txt", "w")
d = enchant.Dict("en_US")
cap = cv2.VideoCapture(0)
while (True):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((1,1), np.uint8)
    gray = cv2.dilate(gray, kernel, iterations=1)
    gray = cv2.erode(gray, kernel, iterations=1)
    gray = cv2.GaussianBlur(gray, (5,5), 0)
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    cropped = gray[260:460, 540:740]
    cv2.imwrite("ronoy.jpg", cropped)
    rawtext = pytesseract.image_to_string(Image.open("ronoy.jpg"))
    processtext = pytesseract.image_to_string(Image.open("ronoy.jpg"))
    processArray = processtext.split(" ")
    cv2.rectangle(gray, (540, 260), (740, 460), (0, 0, 0), 15)
    cv2.imshow('frame',gray)
    for x in processArray:
        tempArray = []
        for i in x:
            if (i.isalpha() == True):
                tempArray.append(i)
        result = ''.join(tempArray)

        if len(result) > 0 and d.check(result.lower()):
            finalFile.write(result)
            finalFile.write("\n")

    print("text", rawtext)
    rawFile.write(rawtext)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
rawFile.close()
finalFile.close()
cap.release()
cv2.destroyAllWindows()
